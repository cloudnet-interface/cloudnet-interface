## Info

Desktop-Interface for the CloudNet-CloudSystem for managing servers and groups,
displaying info, editing your Cloud and much more.

Click [here](https://discord.gg/Bb5r9ER) to join the discord for questions and
suggestions.

## Using

The interface is far from beeing finished and there are not many features
implemented yet, but if you still want to use it and test the current features,
you can download the code and run this commands in the directory of where you
saved it: 

```bash
$ npm install
$ npm start
```

Notice: You need [Node.js](https://nodejs.org) for this.

It's also very important, that you have the [CloudNet-Extension-Module](https://gitlab.com/cloudnet-interface/cloudnet-interface-extension)
installed at your master. The repo also contains a README, where is explained 
how to set the module up and where you can find the info, you'll need to connect to the Cloud.
