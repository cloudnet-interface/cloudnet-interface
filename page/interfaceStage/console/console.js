const socket = require("../../../main/script/socket");

let consoleServerName, serverConsole, commandInput, serverName, serverType;

function cache() {
    if(!consoleServerName) {
        consoleServerName = document.getElementById("consoleServerName");
        serverConsole = document.getElementById("serverConsole");
        commandInput = document.getElementById("commandInput");
    }
}

const messageListener = (queryResult) => {
    const message = queryResult["message"];
    const content = queryResult["content"];
    if(message && content && Array.isArray(content)) {
        if(message === "consoleLine")
            serverConsole.innerText += "\n" + content.join("\n");
    }
};

const keyDownListener = (keyDownEvent) => {
    if(keyDownEvent.key === "Enter" && commandInput.value.trim()) {
        socket.sendSocketMessage(serverType + "command", serverName, commandInput.value.trim());
        commandInput.value = "";
    }
};

function start(server, type) {
    serverName = server;
    serverType = type;
    consoleServerName.innerText = server;
    serverConsole.innerText = "";
    commandInput.addEventListener("keydown", keyDownListener);
    socket.registerListener("message", messageListener);
    socket.sendSocketMessage(type + "consolesession", server);
}

function stop() {
    commandInput.removeEventListener("keydown", keyDownListener);
    socket.sendSocketMessage(serverType + "consolesession", serverName);
    socket.removeListener("message", messageListener);
}

module.exports.cache = cache;
module.exports.start = start;
module.exports.stop = stop;