const loginScreen = require("../../main/screen/loginScreen");
let textInputs, cloudNetAddress, cloudNetUser, cloudNetPassword;

document.addEventListener("DOMContentLoaded", () => {
    // Caching the elements
    textInputs = document.getElementsByTagName("input");
    cloudNetAddress = document.querySelector("input[id=cloudNetAddress]");
    cloudNetUser = document.querySelector("input[id=cloudNetUser]");
    cloudNetPassword = document.querySelector("input[id=cloudNetPassword]");

    // Removing the invalid-class of the inputs when clicked
    for(let i = 0; i < textInputs.length; i++) {
        const textInput = textInputs[i];
        if(textInput.parentElement.classList.contains("input-field")) {
            textInput.addEventListener("click", () => {
                if(textInput.classList.contains("invalid"))
                    textInput.classList.remove("invalid");
            });
        }
    }
});

/**
 * Tries to login with the data from the textInputs
 *
 * @returns {Promise<void>}
 */

async function sendLogin() {
    const address = cloudNetAddress.value;
    const user = cloudNetUser.value;
    const password = cloudNetPassword.value;

    // Checking if one of the fields is empty
    if(!address.trim() || !user.trim() || !password.trim()) {
        M.toast({html: "Es sind nicht alle Felder ausgefüllt"});
        invalidateInputs();
        return;
    }

    await loginScreen.login(address, user, password);
}


/**
 * Gives the inputs the invalid-class
 */

function invalidateInputs() {
    for(let i = 0; i < textInputs.length; i++) {
        const textInput = textInputs[i];
        textInput.classList.add("invalid");
    }
}

exports.sendLogin = sendLogin;
exports.invalidateInputs = invalidateInputs;

